.. _webassistant_configuration:

*************************
Web / Desktop Application
*************************

.. _webassistant_disable_webrtc:

Disabling WebRTC
================

WebRTC can be disabled globally by setting the ``DISABLE_WEBRTC`` environment variable to ``true`` in :file:`/etc/docker/compose/custom.env` file.


.. _webassistant_history_nbdays:

Call History Number of Days
===========================

Call history by default shows the last 7 days. You can change it by setting the ``CALL_HISTORY_NB_OF_DAYS`` environment variable to a specific number of days in the :file:`/etc/docker/compose/custom.env` file. 

.. warning:: Note that setting this to a large number of days may slow down the solution.
