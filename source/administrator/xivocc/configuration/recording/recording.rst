.. _recording_configuration:

***********************
Recording configuration
***********************

This page describes how to configure the recording feature.


Configure recording
===================

To configure recording there are two steps to follow on *XiVO PBX*:

#. Add link towards Recording Server,
#. and then enable recording, which can be done either:

  * in the Queue configuration
  * or via subroutines


1. Add link towards Recording Server
------------------------------------

.. note:: Steps to be done on **XiVO PBX**

The first step is to configure the link towards the Recording Server by running the configuration script:

.. code-block:: bash

  xivocc-recording-config

During the configuration, you will be asked for :

* the Recording Server IP (i.e. 192.168.0.2)
* the *XiVO PBX* name (it must not contain any space or "-" character).
  If you configure more more than one *XiVO PBX* on the same Recording Server, you must give a different name to each of them.

After having configured the recording, you have to enable it via sub-routines. See below.

2. Enable recording
-------------------

.. _queue_recording_configuration:

Enable recording in the Queue configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note:: Steps to be done on **XiVO PBX**

To enable recording on a queue, go to :menuselection:`Services -> Contact Center -> Queues` and edit the queue.

.. figure:: recording_queue.png
    :scale: 90%

Then, in the recording section:

* :guilabel:`Recording mode` set it to *Recorded* or *Recorded on demand*

  * *Recorded*: call will be recorded
  * *Recorded on demand*: recording starts in paused state and can be activated by the agent (see :ref:`agent recording configuration <agent_recording>`)

* :guilabel:`Activate` check it for the recording mode to be active

  * You need to check the :guilabel:`Activate` parameter for the recording to be enabled.
    When :guilabel:`Activate` is checked, the recording will be enabled according to the mode selected.


.. _enable_recording_via_subroutines:

Enable recording via subroutines
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note:: Steps to be done on **XiVO PBX**

To enable the recording you have to configure one of the shipped subroutines.

The package ``xivocc-recording`` (see :ref:`recording installation section <recording_xpbx>`) ships the following dialplan subroutines :

===================================  ==========================
Subroutine                           Description
===================================  ==========================
``xivocc-incall-recording``          Records incoming calls
``xivocc-incall-recording-paused``   Records incoming calls, but record starts in paused state and can be activated by the agent (see :ref:`agent recording configuration <agent_recording>`)
``xivocc-outcall-recording``         Records outgoing calls
``xivocc-outcall-recording-paused``  Records outgoing calls, but record starts in paused state and can be activated by the agent (see :ref:`agent recording configuration <agent_recording>`)
===================================  ==========================

These subroutines are to be configured on the following *XiVO PBX* objects (either globally or per-object):

.. warning:: They **MUST** be configured **only** on the following objects. Other configuration **are not supported**.

* Incalls,
* and/or Users,
* and/or Outcall

----

.. note:: Here is an **example** if you want to enable recording for:

    * *All* outbound calls but started in pause state,
    * And *only on* incoming call 0123456789

    Then you would have to:

  #. Create a ``custom_global_subr.conf`` file in the ``/etc/asterisk/extensions_extra.d`` directory
  #. If not already defined elsewhere define the global subroutine::

          [xivo-subrgbl-outcall]
          exten = s,1,NoOp(=== Recording outbound calls in pause ===)
          same = n,Gosub(xivocc-outcall-recording-paused,s,1)
          same = n,Return()

  #. Enable the call recording for incall 0123456789 by editing it via the *XiVO PBX* web interface
     and set the field :menuselection:`Pre-process subroutine` to ``xivocc-incall-recording``


.. _stop_recording_upon_ext_xfer_conf:

Stop recording upon external transfer
-------------------------------------

By default recording is stopped when both parties of the call are external.

This can be deactivated by adding ``STOP_RECORDING_UPON_EXTERNAL_XFER`` environment variable to the xuc section of your :file:`docker-xivocc.yml` file:


.. code-block:: yaml
  :emphasize-lines: 1,7

  xuc:
    image: ...

    environment:
    - ...
    - SECURED_KRB5_PRINCIPAL
    - STOP_RECORDING_UPON_EXTERNAL_XFER


and ``STOP_RECORDING_UPON_EXTERNAL_XFER=false`` value to your :file:`custom.env`.

.. code-block:: bash
  :emphasize-lines: 5

  XIVO_HOST=192.168.1.1
  XUC_HOST=192.168.1.2
  XUC_PORT=8090
  ...
  STOP_RECORDING_UPON_EXTERNAL_XFER=false

and then relaunch the xivocc services with ``xivocc-dcomp up -d`` command.


.. _recording_filtering_configuration:

Recording filtering configuration
=================================

.. note:: Steps to be done on **XiVO CC**

After having followed above paragraphs, you can also configure the recording filtering.

#. Add a user with Administrateur rights for Recording Server:

   #. Connect to the Config Management interface : http://<XIVO_CC_IP>:9100 (login avencall/superpass),
   #. Add one of the *XiVO PBX* user giving him *Administrateur* rights,

#. Configure excluded numbers on Recording Server

   #. Then, connect with this user to the Recording Server interface : http://<XIVO_CC_IP>:9400
   #. Navigate to the page ``Contrôle d'enregistrement`` and add the numbers to be excluded from the recording.


In list :menuselection:`Destinataire de l'appel (Numéro entrant, File d'attente, Utilisateur)` declare the:

* XiVO Incalls numbers,
* XiVO Queues numbers,
* or XiVO Users numbers

to be excluded from the recording on incoming or internal call. These numbers will be checked by the ``xivocc-incall-recording`` subroutines.

.. note:: numbers must be entered as they first appear in dialplan (check is made against XIVO_DSTNUM dialplan variable).

In list :menuselection:`Emetteur ou destinataire d'un appel sortant (Utilisateur ou numéro appelé externe)` declare the:

* XiVO Users internal numbers

to be excluded from recording on outgoing calls. These numbers will be checked by the ``xivocc-outcall-recording`` subroutines.

.. note:: check is made against XIVO_SRCNUM dialplan variable.


.. _recording_gw_configuration:

********************
Recording on gateway
********************

Recording can be enabled on a gateway instead of the XiVO PBX where the users, queues and other objects are configured. This architecture will allow to off-load the recording process to a gateway server. To configure recording on a gateway, you need to follow the process to enable recording via subroutines (see :ref:`enable_recording_via_subroutines`).

In order to allow agents to control the recording of their current call, you need to perform the following steps:

* Copy the following file `/etc/asterisk/manager.d/02-xivocc.conf` from the XiVO PBX to the same folder on the gateway.
* On your XiVO gateway, configure your operators' trunks and the trunk to your XiVO [1]_.
* On your XiVO PBX, configure the gateway as a media server in `Configuration/Media servers`
* On your XiVO PBX, configure your operators' trunks of your gateway and assign it to the media server configured for your XiVO gateway [1]_.
* Disable the feature to stop recording upon external transfer (see :ref:`stop_recording_upon_ext_xfer_conf`, `STOP_RECORDING_UPON_EXTERNAL_XFER=false`)
* Configure recording as per :ref:`enable_recording_via_subroutines`

.. [1] Please note that you need to do this configuration both on the XiVO PBX and on your gateway as no synchronization mechanism exists at the moment.
