*********
Reporting
*********

.. contents::

.. _totem_panels:

Totem Panels
============

The *XiVO CC* comes with the ELK [1]_ stack. This offers the possibility to do some dashboard to visualize, for example, your call center activity.

This can also be used to explore your data and analyze it.

Example:

.. figure:: kibana_dashboard_ccagent_example.png


.. seealso::

  * :ref:`totem_panels_configuration` section to have the demo dashboard.
  * :ref:`totem_data_flow` section to learn about what is put inside Elasticsearch.

.. [1] ELK: Elasticsearch, Logstash, Kibana - see https://www.elastic.co


.. _totem_panels_configuration:

Import Default Configuration and Demo Dashboards
------------------------------------------------

Preconfigured panels and index pattern are available on our gitlab repository.

#. Download the sample panels from https://gitlab.com/xivocc/sample_reports/blob/master/kibana-set/kibana.json
#. Import this json file in Kibana:

  * Goto :menuselection:`Kibana management --> Saved objects`` menu

     .. figure:: kibana_mngt.png

  * Click on :menuselection:`Import` button,
  * In the new menu select the previously :file:`kibana.json` downloaded file
  * If not checked, activate the *Automatically overwrite all saved object ?* toggle
  * Click on *Import* button

You can now browse the sample reports in :menuselection:`Dashboard`

.. figure:: kibana_dashboardmenu.png


Note that to work on your environment, the *CC - Queues* dashboard must edited to change the queues name.


.. _totem_data_flow:

Data flow
---------

Data (i.e. the ``queue_log`` table) is replicated:

* from the *XiVO PBX* to the *XiVO CC* with the ``db_replic`` service (see logs in XiVO in :file:`/var/log/xivo-db-replication/xivo-db-replication.log`)

  * *Note:* this is done almost instantaneously

* from the database of the *XiVO CC* to Elasticsearch with Logstash

  * *Note:* this is done every 1 minute

* Elasticsearch is configured to keep at least 30 days of data.


.. _totem_check_status:

Check the status
----------------

To check the ELK stack:

* Go to the Kibana monitoring page

  .. figure:: kibana_monitoring.png

* Activate the monitoring if it wasn't:

  .. figure:: kibana_monitoring_activate.png

* And then you can see

  .. figure:: kibana_monitoring_page.png


