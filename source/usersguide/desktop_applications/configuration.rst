.. _desktop-application-configuration:

*************
Configuration
*************


On first launch the application will display the settings page and ask for the application server.
Basically it should be the IP address of your server
If you don't know it, you need to ask your system administrator or refer to :ref:`dapp_protocol_app_server` paragraph.


Navigation
==========

.. figure:: navigation.png
   :scale: 100%

The top menu (accessible through tray icon or right click on the logo) allows you to navigate either to the application or to the settings page. If you did not enter any setting, the application will redirect you to the settings page.


About
=====

By clicking the **?** menu you will open a popup that show you technical information about the application that can be used to report bugs.


Settings
========

Settings page is either accessible from top menu or by clicking directly on the wrench icon in topmost right bar.

.. figure:: settings.png
   :scale: 100%

.. _dapp_user_interface:

User Interface
--------------

Choose if you want to use the Desktop Assistant to access the UC Assistant application or the CC Agent application.

Application Options
-------------------

* **Launch at startup** if enabled, the app starts automatically when you log in to your machine.
* **Close in tray** if enabled, the app stays running in the notification area after app window is closed.


.. _dapp_global_key:

Global keyboard shortcut and Select2Call
----------------------------------------

This field allow you to define one shortcut in application to handle all basic actions that can be done for managing calls. With one combination keypress you should be able to:

* **Call** the phone number contained in your clipboard (a.k.a **Select2Call**)
* **Answer** a call if phone is ringing
* **Hangup** if you are already on call

.. note::
  | To be able to **call** someone, you **must** beforehand have copied in your clipboard a phone number from any source (web page, e-mail, text document...)

   * **Linux**: select phone number then trigger `shortcut`
   * **Windows**:  select phone number, type ``Ctrl+C`` then trigger `shortcut`

  Default **Select2Call** shortcut is ``Ctrl+Space`` for **Windows** and **Linux**, you can either change it or disable it by leaving the field blank.

.. warning:: You must be logged in for using global shortcut and automatic dialing to work.


.. _dapp_protocol_app_server:

Protocol and Application Server
-------------------------------

In these two fields you need to specify the protocol and address to reach the application.
The table below list the possible value:

+--------------------------------+------------------------------------------------------------+
|                                |                      *Connection URL*                      |
+--------------------------------+--------------------------+---------------------------------+
| *Protocol*                     | **Secure (recommended)** | Non Secure (should not be used) |
+--------------------------------+--------------------------+---------------------------------+
| *Application server*           | ``XiVOCC_IP``            | ``XiVOCC_IP:8070``              |
+--------------------------------+--------------------------+---------------------------------+

Note that ``XiVOCC_IP`` or ``XiVOCC_IP:8070`` can be replaced by a FQDN if your administrator has set one and must **not** be prefixed by a protocol (e.g. https)

.. _dapp_call_url:

Handling callto: and tel: URLs
------------------------------

The *Desktop Application* can handle telephone number links that appear in web pages. The *Desktop Application* will
automatically dial the number when you click on a link.

It is supported on both Windows and Linux Debian based distributions (with a desktop environment compatible with Freedesktop_).

.. _Freedesktop: https://www.freedesktop.org/wiki/


.. _dapp_inifile:

Config file
-----------

You can specify your Desktop Assistant parameters by providing a :file:`xivoconfig.ini` file or by editing the one that gets created automatically
on first launch. This file **must be located in the same folder as your Desktop Assistant executable**.

Below is an example of the file:`xivoconfig.ini` file content:

.. code-block:: ini

 APP_PROTOCOL=HTTPS
 APP_SERVER=xivo.mycorp.com
 APP_INTERFACE=UCASSISTANT
 APP_STARTUP=false
 APP_CLOSE=true

APP_PROTOCOL
   Define the use of HTTPS or HTTP connection.
   Values: ``HTTP`` or ``HTTPS`` (default to ``HTTPS``)

APP_SERVER
   The server used by the Desktop Assistant.
   Either an URL (e.g. xivo.mycorp.com) or an IP:PORT (e.g. 192.168.0.1:8070)

APP_INTERFACE
   Which application to open between UC Assistant or CC Agent.
   Values: ``UCASSISTANT`` or ``CCAGENT``

APP_STARTUP (optional)
   Enable open at startup of OS.
   Values: ``true`` or ``false``

APP_CLOSE (optional)
   Enable minimization in the taskbar when closing the application.
   Values: ``true`` or ``false``


Update
======

On Windows, the application will check at startup for a new version of the application and offer to upgrade if one is available.

.. figure:: update.png
   :scale: 100%


On Debian, the update relies on the package manager behavior. However you can check for any update by issuing the following commands:

.. code-block:: bash

                sudo apt-get update
                apt-cache policy xivo-desktop-assistant


Startup options
===============

The Desktop Application can be started with following options:

* ``--ignore-certificate-errors`` to disable certificate verification, this option is meant **only** for test purposes. You can use it with self-signed certificates.
* ``-d`` to enable debug menu items
* ``-t`` with an authentication token as parameter (see :ref:`dapp_autolog_wtoken`)

.. note:: On **Windows**  both options must be set to the shortcut ``xivo-desktop-assistant.exe`` pointing to application located in ``C:\Users\<USER>\AppData\Local\xivo\xivo-desktop-assistant.exe`` so that **Target** of shortcut looks like for example to:
  ``C:\Users\IEUser\AppData\Local\xivo\xivo-desktop-assistant.exe --ignore-certificate-errors -d``

.. _dapp_autolog_wtoken:

Autologin with a token
----------------------

You can autolog an user using his user token.

Pre-requisites:

#. Create a :file:`xivoconfig.ini` file - see :ref:`dapp_inifile` - with at least the ``APP_PROTOCOL``, ``APP_SERVER`` and ``APP_INTERFACE`` parameters
#. Retrieve the user token (you can retrieve it using curl - see :ref:`rest_authentication`)

When you have the token, you can pass it to the :program:`xivo-desktop-assistant` exe using arg ``-t YOUR_TOKEN``


Custom user data directory
--------------------------

It is possible to set an environment variable named ``CUSTOM_USER_DATA`` where will be stored application configuration. This is usefull if you want for example two shortcuts with two distincts configuration (one for `UC Assistant` and one for `CC Agent`) on the same desktop application installed.

On **Windows** here an example of two shortcuts that set the environment variable (**uc** or **cc**) and launch the application. Following lines must be updated with your **Windows** user name and correct `XiVO` version and must be set in ``Target`` field of the shortcut).

`UC assistant`:

.. code-block:: none

  C:\Windows\System32\cmd.exe /V /C "SET CUSTOM_USER_DATA=C:/Users/IEUser/AppData/Local/xivo-desktop-assistant/uc&& START /D ^"C:\Users\IEUser\AppData\Local\xivo-desktop-assistant\app-2018.7.0^" xivo-desktop-assistant.exe"

`CC ccagent`:

.. code-block:: none

  C:\Windows\System32\cmd.exe /V /C "SET CUSTOM_USER_DATA=C:/Users/IEUser/AppData/Local/xivo-desktop-assistant/cc&& START /D ^"C:\Users\IEUser\AppData\Local\xivo-desktop-assistant\app-2018.7.0^" xivo-desktop-assistant.exe"

.. note:: You can set `Run` mode to `Minimized` in shortcut `General` tab to avoid **cmd.exe** blinking at startup.

Known limitations
=================

* Click on links using protocol `tel:` on **Windows** may not work if any version of `Skype` / `Lync` is installed on the PC.

.. _desktop_troubleshoot:

Troubleshoot Application
========================

If you don't succeed to reach login page of desired application (i.e. just give you the possibility to retry or to change parameters) and if you observe some errors about certificate in debug mode, you should

* Check that you installed correctly the certificate under `/etc/docker/nginx/ssl` (:ref:`webrtc-ssl-cert`).
* Take care if your move a `*.cer` to `*.crt`. You must concatenate a key file to your `*.cer` (``cat certifcate.cer certificate.key > certificate.crt``). Just rename it will not work
* Check that XUC_HOST in `/etc/docker/compose/custom.env` is also configured with the same FQDN as in the certificate, not the IP address.
