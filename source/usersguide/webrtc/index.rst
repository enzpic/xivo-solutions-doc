.. _webrtc_requirements:

******************
WebRTC Environment
******************

One can use WebRTC with *XiVO PBX* and *XiVO CC* in the following environment:

* LAN network (currently no support for WAN environment),
* with the:

  * *UC Assistant* or *CC Agent* with Chrome browser version **57** or later
  * or *Desktop Application*

Requirements
============

The **requirements** are:

* to have a microphone and headphones for your PC,
* to configure your *XiVO PBX*:

  * configure :ref:`asterisk_http_server`,
  * and then create users with a WebRTC line (see: :ref:`configure_user_with_webrtc_line`),

* have a SSL/TLS certificate signed by a certification authority installed on the nginx of *XiVO CC* (see: :ref:`webrtc-ssl-cert`),
* and use *https*:

  * *UC Assistant*: you must connect to the *UC Assistant* via `https` protocol,
  * *Desktop Application*: you must check *Protocol -> Secure* in the application parameters.

.. note:: Currently you can not have a user configured for both WebRTC and a phone set at the same time.

Limitations
===========

Known limitation are :

* Voice may not be able to hear if your computer have more than 4 network interfaces up at the same time (this can happen if you use virtualization)

.. note::

  | To check if you have more than 4 network interfaces you can type following command:
  | ``ls /sys/class/net``

  | Then just use:
  | ``ifdown <ifname>``

  | This will switch off network interface not required to make your call.

Additional chrome WebRTC-specific options
==========================================

There are various additional settings used in the code.
They are used to improve audio quality by enabling or disabling chrome WebRTC-specific flags.

.. note::

  | These options are not customisable. They are set in the code.

**Chrome currently supports these audio quality options :**

* **Automatic gain control** :  Adjust voice sound level to make it linear, lowering sound level when the user speaks too loudly.

* **Echo cancellation** : Detect and delete echo coming from the playback of the user's own voice.

* **Noise suppression** : Cancel background noises coming from the user's environment.

* **Highpass filter** : Filters out low frequencies noises (like microphone background buzzing permanent sound).

* **Audio mirroring** : Reflect sound coming from different directions into a focus point (similar to a parabola).

* **Typing noise detection** : Detect and delete keypress sounds.

**The current production code is set as follows :**

* googAutoGainControl is set to *false*
* googAutoGainControl2 is set to *false*
* googEchoCancellation is set to *true*
* googEchoCancellation2 is set to *true*
* googNoiseSuppression is set to *false*
* googNoiseSuppression2 is set to *false*
* googHighpassFilter is set to *false*
* googAudioMirroring is set to *false*
* googTypingNoiseDetection is set to *true*

.. note::

  | This flag used to be valid but is now deprecated :
  | - Ducking : Reduce an audio signal by the presence of another signal (multiple people talking at the same time).