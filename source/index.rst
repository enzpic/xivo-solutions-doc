.. XiVO-doc Documentation master file.

******************************************************
XiVO Solutions |version| Documentation (Deneb Edition)
******************************************************

.. warning:: This version is not yet release. See `current LTS <https://documentation.xivo.solutions/en/2019.05/>`_ documentation.

.. important:: **What's new in this version ?**

  * Call control display enhancement for UC,
  * Instant Messaging with conversation list,
  * Switch parameter in Desktop Assistant for UC Assistant or CC Agent,
  * Activity's failed destination management from CC Agent and CC Manager,
  * 3-parties conference for WebRTC,
  * WebRTC user import feature,
  * Contacts in Phonebook can be added with a leading `+` character,
  * ELK 7 for reporting.

  See :ref:`deneb_release` page for the complete list of **New Features** and **Behavior Changes**.


.. figure:: logo_xivo.png
   :scale: 70%

XiVO solutions developed by Avencall_ is a suite of PBX applications based on several free existing components including Asterisk_
and our own developments. This powerful and scalable solution offers a set of features for corporate telephony and call centers to power their business.

You may also have a look at our `development blog <http://xivo-solutions-blog.gitlab.io/>`_ for technical news about the solution

.. _Asterisk: http://www.asterisk.org/
.. _Avencall: https://www.xivo.solutions/


.. toctree::
   :maxdepth: 2

   introduction/introduction
   getting_started/getting_started
   installation/index
   administrator/index
   ipbx_configuration/administration
   contact_center/contact_center
   Centralized User Management <centralized_user_management/index>
   usersguide/index
   Devices <https://documentation.xivo.solutions/projects/devices/en/latest/>
   api_sdk/api_sdk
   contributing/contributing
   releasenotes/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
