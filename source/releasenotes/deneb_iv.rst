********************************
XiVO Deneb Intermediate Versions
********************************

2019.11
=======

Consult the `2019.11 Roadmap <https://projects.xivo.solutions/versions/158>`_.

Components updated: **packaging**, **recording-server**, **xivo-confd**, **xivo-config**, **xivo-db**, **xivo-dird**, **xivo-dist**, **xivo-full-stats**, **xivo-install-script**, **xivo-monitoring**, **xivo-swagger-doc**, **xivo-upgrade**, **xivocc-installer**, **xucmgt**, **xucserver**

**CCAgent**

* `#2826 <https://projects.xivo.solutions/issues/2826>`_ - Chrome / Chromium crashes when agent misses a call and browser has its focus on another tab

**CCManager**

* `#2711 <https://projects.xivo.solutions/issues/2711>`_ - As a manager I want to change the destination of the exceptional closing for a given queue
* `#2799 <https://projects.xivo.solutions/issues/2799>`_ - Respect dissuasion rights in the ccmanager

**Desktop Assistant**

* `#2771 <https://projects.xivo.solutions/issues/2771>`_ - UCAssistant to CCAgent doesn't refresh
* `#2847 <https://projects.xivo.solutions/issues/2847>`_ - Electron isn't displayed properly on high dpi screens

**Recording**

* `#2760 <https://projects.xivo.solutions/issues/2760>`_ - improve recording server login page to match other login pages

**Reporting**

* `#2790 <https://projects.xivo.solutions/issues/2790>`_ - Update call transfers model from cels using streams
* `#2794 <https://projects.xivo.solutions/issues/2794>`_ - Add caller callee numbers to xc_call_channels
* `#2795 <https://projects.xivo.solutions/issues/2795>`_ - Add ring_duration to xc_call_channels
* `#2796 <https://projects.xivo.solutions/issues/2796>`_ - Add origin_call_id in xc_call_channel
* `#2802 <https://projects.xivo.solutions/issues/2802>`_ - ELK - Enhance logstash SQL request
* `#2803 <https://projects.xivo.solutions/issues/2803>`_ - ELK - Data in Elasticsearch should be cleaned up after sometime

  .. important:: **Behavior change** If you are upgrading from the version 2019.10 you MUST run the following command on your CC **before upgrade** to delete the old Elastic indexes::

      curl -X DELETE 'http://localhost:9200/_all'

    **After upgrade** you MUST import the default Kibana configuration and demo dashboards : see :ref:`totem_panels_configuration`

* `#2839 <https://projects.xivo.solutions/issues/2839>`_ - ELK - Add all queue_log column to request
* `#2841 <https://projects.xivo.solutions/issues/2841>`_ - add original_call_id to xc_call_channel when queue call is linked to a consultation call
* `#2855 <https://projects.xivo.solutions/issues/2855>`_ - ELK - Limit logstash RAM footprint

**SpagoBi**

* `#2742 <https://projects.xivo.solutions/issues/2742>`_ - SpagoBI build sometimes doesn't install patches
* `#2767 <https://projects.xivo.solutions/issues/2767>`_ - Update default session-timeout in SpagoBI

**Web Assistant**

* `#2759 <https://projects.xivo.solutions/issues/2759>`_ - Improve flashtext errors message to make them easier for users
* `#2768 <https://projects.xivo.solutions/issues/2768>`_ - Web assistant history display issue
* `#2775 <https://projects.xivo.solutions/issues/2775>`_ - display more calls in call history
* `#2784 <https://projects.xivo.solutions/issues/2784>`_ - create the flashtext history and improve flashtext ui overall
* `#2812 <https://projects.xivo.solutions/issues/2812>`_ - fix voicemail notifictaion icon
* `#2824 <https://projects.xivo.solutions/issues/2824>`_ - Flashtext 'new message' badge display even when the messages are read
* `#2827 <https://projects.xivo.solutions/issues/2827>`_ - Flashtext/Chat improvements
* `#2828 <https://projects.xivo.solutions/issues/2828>`_ - QoL key binding to send message and auto focus on text box
* `#2830 <https://projects.xivo.solutions/issues/2830>`_ - Add the complete date of a message on hover while in party chat
* `#2835 <https://projects.xivo.solutions/issues/2835>`_ - Add return mechanism to party chat
* `#2844 <https://projects.xivo.solutions/issues/2844>`_ - Can compose a message by pressing shift enter, fix white line pb
* `#2846 <https://projects.xivo.solutions/issues/2846>`_ - Modify the style of some elements in the Web assistant
* `#2854 <https://projects.xivo.solutions/issues/2854>`_ - CSS modifications in the Web assistant

**XUC Server**

* `#2848 <https://projects.xivo.solutions/issues/2848>`_ - Cannot change pause reason when already on pause

**XiVO PBX**

* `#2813 <https://projects.xivo.solutions/issues/2813>`_ - Add next LTS name (electra) in xivo-dist
* `#2821 <https://projects.xivo.solutions/issues/2821>`_ - Remove nginx and xivo-web-interface packages
* `#2823 <https://projects.xivo.solutions/issues/2823>`_ - Error in xivo-dcomp script if used for custom commands

**XiVOCC Infra**

* `#2809 <https://projects.xivo.solutions/issues/2809>`_ - xivocc-installer fails when docker 1.12 is installed
* `#2810 <https://projects.xivo.solutions/issues/2810>`_ - Update xivocc-installer docker dependency

2019.10
=======

Consult the `2019.10 Roadmap <https://projects.xivo.solutions/versions/157>`_.

Components updated: **asterisk**, **config-mgt**, **dahdi-linux**, **recording-server**, **xivo**, **xivo-confgend**, **xivo-full-stats**, **xivo-install-script**, **xivo-service**, **xivo-upgrade**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**

**Asterisk**

* `#2730 <https://projects.xivo.solutions/issues/2730>`_ - Asterisk doesn't always fill the hangupsource field

**CCAgent**

* `#2764 <https://projects.xivo.solutions/issues/2764>`_ - Adding the right to change exceptional closing dissuasion for agents who have an administrator profile
* `#2797 <https://projects.xivo.solutions/issues/2797>`_ - In Activity panel, Diss. title should not be displayed if Agent has no dissuasion rights

**CCManager**

* `#2781 <https://projects.xivo.solutions/issues/2781>`_ - UI - Turning ECD implementation into a directive so it becomes reusable
* `#2788 <https://projects.xivo.solutions/issues/2788>`_ - Adding dissuasion into CCManager
* `#2805 <https://projects.xivo.solutions/issues/2805>`_ - Dissuasion menu broken in ccmanager

**Config mgt**

* `#2786 <https://projects.xivo.solutions/issues/2786>`_ - ConfigMGT - Add sample page for testing

**Desktop Assistant**

* `#2763 <https://projects.xivo.solutions/issues/2763>`_ - Autolog user using token

**Recording**

* `#2755 <https://projects.xivo.solutions/issues/2755>`_ - Recording server - Add sample page for testing
* `#2780 <https://projects.xivo.solutions/issues/2780>`_ - Recording server - Add remaining tests on the sample page

**Reporting**

* `#2752 <https://projects.xivo.solutions/issues/2752>`_ - Improve xc_call_channel to fill user_id value when agent took the call
* `#2777 <https://projects.xivo.solutions/issues/2777>`_ - Improve creating the conversations to follow all transfers
* `#2787 <https://projects.xivo.solutions/issues/2787>`_ - xc_call_channel queue_call_id is not always set when call is timeout in queue
* `#2798 <https://projects.xivo.solutions/issues/2798>`_ - Blind transfer not correctly processed in queue call and call channel

**Web Assistant**

* `#2761 <https://projects.xivo.solutions/issues/2761>`_ - move the call control part into the uc header

  .. important:: **Behavior change** The call control tab is not a tab anymore, it's now permanently accessible in the header, it only appears when there is one or more calls ongoing and the button to access that tab does not exist anymore

* `#2782 <https://projects.xivo.solutions/issues/2782>`_ - user call forwarding duplicates when ticking the box

**XiVO PBX**

* `#2684 <https://projects.xivo.solutions/issues/2684>`_ - Dockerization of the XiVO Web Interface

  .. important:: **Behavior change** This intermediate version can't be used with XiVO UC add-on

* `#2736 <https://projects.xivo.solutions/issues/2736>`_ - dahdi-linux-dkms debian package install git repository files
* `#2778 <https://projects.xivo.solutions/issues/2778>`_ - xivo-webi cannot display routes if your are on Edge or Chrome < v69

**XiVO Provisioning**

* `#2603 <https://projects.xivo.solutions/issues/2603>`_ - Support of Snom D717
* `#2604 <https://projects.xivo.solutions/issues/2604>`_ - Support of Snom D735
* `#2605 <https://projects.xivo.solutions/issues/2605>`_ - Support of Snom D785

**XiVOCC Infra**

* `#2682 <https://projects.xivo.solutions/issues/2682>`_ - ELK7 in XivoCC

  .. important:: **Behavior change** Postgresql data are not in a volume anymore, we migrate them to the host machine, you can remove the pgxivocc container without any harm.
    ELK stack was migrated to the version 7, there's a manual procedure to temporarily enable the previous version.
    Elasticseach is feeded by Logstash, the db_replic is not used for Elasticsearch anymore.
    See :ref:`elk7_upgrade_notes`.

* `#2770 <https://projects.xivo.solutions/issues/2770>`_ - Nginx not recreated when xucmgt is recreated


2019.09
=======

Consult the `2019.09 Roadmap <https://projects.xivo.solutions/versions/156>`_.

Components updated: **config-mgt**, **recording-server**, **xivo-dao**, **xivo-db**, **xivo-dird**, **xivo-full-stats**, **xivo-outcall**, **xivo-stat**, **xivo-upgrade**, **xivo-web-interface**, **xucmgt**, **xucserver**

**CCAgent**

* `#2647 <https://projects.xivo.solutions/issues/2647>`_ - Managing queue exceptional closing destination
* `#2668 <https://projects.xivo.solutions/issues/2668>`_ - As an Agent I can see/change the ECD of the queue if I have the right
* `#2739 <https://projects.xivo.solutions/issues/2739>`_ - CCAgent - As an agent, I can only see the ECDs and be able to update them if I have the right

  .. important:: **Behavior change** Exceptional closing dissuasion now can only be set by supervisor having the corresponding right set in configmgt

* `#2753 <https://projects.xivo.solutions/issues/2753>`_ - Webrtc agent is still ringing after logged out on CC agent on login page master

**Config mgt**

* `#2731 <https://projects.xivo.solutions/issues/2731>`_ - Configmgt - Add dissuasionAccess in the rights API for administrators and supervisors
* `#2732 <https://projects.xivo.solutions/issues/2732>`_ - Configmgt - Add dissuasion rights management in the web interface
* `#2734 <https://projects.xivo.solutions/issues/2734>`_ - Configmgt doc - Update existing doc for rights & add updateDissuasion right

**Desktop Assistant**

* `#2671 <https://projects.xivo.solutions/issues/2671>`_ - As a user I want to be able to switch between UC Assistant and CCAgent with the desktop assistant

  .. important:: **Behavior change** You can now choose between UCAssistant and CCAgent with desktop assistant settings. It also get removed from the settings 'server' field.

* `#2726 <https://projects.xivo.solutions/issues/2726>`_ - rework our ipc dependant mechanics and upgrade to electron 6.0.2

**Recording**

* `#2674 <https://projects.xivo.solutions/issues/2674>`_ - We should display one week of history (recording-server part)
* `#2740 <https://projects.xivo.solutions/issues/2740>`_ - Recording Server - Add dissuasion to supervisor rights

**Reporting**

* `#2703 <https://projects.xivo.solutions/issues/2703>`_ - Update conversation data model using streams
* `#2748 <https://projects.xivo.solutions/issues/2748>`_ - End time is not updated for func keys, pickup calls and dahdi channels

**Web Assistant**

* `#2743 <https://projects.xivo.solutions/issues/2743>`_ - No results when listing all users in assistant
* `#2745 <https://projects.xivo.solutions/issues/2745>`_ - Aligment issues in call history of ucassistant

**XUC Server**

* `#2706 <https://projects.xivo.solutions/issues/2706>`_ - We should display one week of history (XUC part)
* `#2738 <https://projects.xivo.solutions/issues/2738>`_ - XUC - Manage user rights when querying the ConfigMGT for dissuasion-related operations

**XiVO PBX**

* `#2644 <https://projects.xivo.solutions/issues/2644>`_ - As a XiVO Admin I can give to an agent the right to change the exceptional closing destination of a specific queue
* `#2733 <https://projects.xivo.solutions/issues/2733>`_ - Add a dissuasion value in the type right_type in the database
* `#2747 <https://projects.xivo.solutions/issues/2747>`_ - Remove statistics  from the web interface
* `#2754 <https://projects.xivo.solutions/issues/2754>`_ - CallerID doesn't work with an external PBX
* `#2756 <https://projects.xivo.solutions/issues/2756>`_ - Allow to use space in Xivo users directory search

2019.08
=======

Consult the `2019.08 Roadmap <https://projects.xivo.solutions/versions/153>`_.

Components updated: **config-mgt**, **xivo-full-stats**, **xivo-upgrade**, **xivocc-installer**, **xucmgt**, **xucserver**

**CCAgent**

* `#2709 <https://projects.xivo.solutions/issues/2709>`_ - CCAgent - Agent can change the destination of the exceptional closing for a given queue to the default queue
* `#2715 <https://projects.xivo.solutions/issues/2715>`_ - CCAgent - Agent can see if the default dissuasion queue is selected

**Config mgt**

* `#2707 <https://projects.xivo.solutions/issues/2707>`_ - Configmgt - Add API to set to a queue the destination of the exceptional closing for a given queue
* `#2713 <https://projects.xivo.solutions/issues/2713>`_ - Configmgt - Make the QueueDissuasion APIs safe by requiring a token to make queries
* `#2714 <https://projects.xivo.solutions/issues/2714>`_ - Configmgt - Change API to be able to return the queue selected for an other queue dissuasion

**Reporting**

* `#2700 <https://projects.xivo.solutions/issues/2700>`_ - Update queue call data model from queuelogs using streams
* `#2701 <https://projects.xivo.solutions/issues/2701>`_ - Improve the hangup action for the call channel data model
* `#2725 <https://projects.xivo.solutions/issues/2725>`_ - Improve the answer time for the queue call data model
* `#2735 <https://projects.xivo.solutions/issues/2735>`_ - Improve the end time for the queue call data model

**Web Assistant**

* `#2675 <https://projects.xivo.solutions/issues/2675>`_ - In history, aggregate lines of successive call of same caller

**XUC Server**

* `#2651 <https://projects.xivo.solutions/issues/2651>`_ - XUC API - Subscribe to phone hint for specific extension and/or list of extension
* `#2708 <https://projects.xivo.solutions/issues/2708>`_ - Xuc - Be able to set the fail destination of a given queue to a other queue

**XiVO PBX**

* `#2643 <https://projects.xivo.solutions/issues/2643>`_ - As a XiVO Admin I can configure the queue which will be one of the exceptional closing destination of the queue
* `#2729 <https://projects.xivo.solutions/issues/2729>`_ - Download of Docker GPG key does not work behind a proxy

**XiVO Provisioning**

* `#2562 <https://projects.xivo.solutions/issues/2562>`_ - Astraa firmware link for fw 3.3.1 does not work anymore
* `#2693 <https://projects.xivo.solutions/issues/2693>`_ - Astraa Firmware 4.3.0 (68XXi-fw) link dead

2019.07
=======

Consult the `2019.07 Roadmap <https://projects.xivo.solutions/versions/149>`_.

Components updated: **config-mgt**, **xivo-config**, **xivo-full-stats**, **xivo-manage-db**, **xivo-provd-plugins**, **xivo-solutions-doc**, **xivo-upgrade**, **xivocc-installer**, **xucmgt**, **xucserver**

**CCAgent**

* `#2646 <https://projects.xivo.solutions/issues/2646>`_ - As an agent logged in CCAgent I can change the sound file destination of the exceptional closing of my queues
* `#2679 <https://projects.xivo.solutions/issues/2679>`_ - CCAgent - Be able to see the list of dissuasion sound file for a queue
* `#2688 <https://projects.xivo.solutions/issues/2688>`_ - [C] - Cannot scroll to the end of content panel when receiving a call
* `#2697 <https://projects.xivo.solutions/issues/2697>`_ - CCAgent - Agent can change the sound file destination of the exceptional closing for a given queue

**Config mgt**

* `#2670 <https://projects.xivo.solutions/issues/2670>`_ - Configmgt - Add API to get the list of sound file destination of the exceptional closing for a given queue
* `#2698 <https://projects.xivo.solutions/issues/2698>`_ - Configmgt - Add API to change the sound file destination of the exceptional closing for a given queue

**Reporting**

* `#2666 <https://projects.xivo.solutions/issues/2666>`_ - Generate new datamodel in database to ease jasper reports creation
* `#2677 <https://projects.xivo.solutions/issues/2677>`_ - Update call_channel data model from cels using streams
* `#2685 <https://projects.xivo.solutions/issues/2685>`_ - Improve the answer action for the call channel data model

**Web Assistant**

* `#2683 <https://projects.xivo.solutions/issues/2683>`_ - Search field in assistant and agent modifies the input

**XUC Server**

* `#2678 <https://projects.xivo.solutions/issues/2678>`_ - Xuc - Be able to get the list of Fail destinations of a given queue
* `#2699 <https://projects.xivo.solutions/issues/2699>`_ - Xuc - Be able to set the fail destination sound file of a given queue

**XiVO PBX**

* `#2654 <https://projects.xivo.solutions/issues/2654>`_ - As a logged agent in CCAgent I can display the list of sound file destination of the exceptional closing of my queues
* `#2669 <https://projects.xivo.solutions/issues/2669>`_ - Logrotate configuration for /var/log/postgresql/ should be created on fresh install and upgrade
* `#2681 <https://projects.xivo.solutions/issues/2681>`_ - Libs LDAP/RESTAPI - be able to update user without using default values
* `#2689 <https://projects.xivo.solutions/issues/2689>`_ - Hostname change is not taken into account

**XiVO Provisioning**

* `#2171 <https://projects.xivo.solutions/issues/2171>`_ - Support of Yealink T40G

**XiVOCC Infra**

* `#2128 <https://projects.xivo.solutions/issues/2128>`_ - XDS - Document to add xuc VoIP address to AMI configuration
* `#2665 <https://projects.xivo.solutions/issues/2665>`_ - XiVO CC installation breaks the main XiVO PBX install if FQDN entered as xuc host
* `#2694 <https://projects.xivo.solutions/issues/2694>`_ - XiVO UC Installer does not set correct pgxivocc address in xivo/custom.env for db_replic

2019.06
=======

Consult the `2019.06 Roadmap <https://projects.xivo.solutions/versions/147>`_.

Components updated: **config-mgt**, **xivo-full-stats**, **xivo-manage-db**, **xucmgt**, **xucserver**

**CCAgent**

* `#2645 <https://projects.xivo.solutions/issues/2645>`_ - As a logged agent in CCAgent I can display the sound file destination of the exceptional closing of my queues
* `#2655 <https://projects.xivo.solutions/issues/2655>`_ - Integrate external directory service inside ccagent content panel
* `#2657 <https://projects.xivo.solutions/issues/2657>`_ - UI - Display failed destination of the queue (if it is a sound file)

**Config mgt**

* `#2656 <https://projects.xivo.solutions/issues/2656>`_ - Configmgt - Add API to get the Fail sound file destination of a queue

**Reporting**

* `#2667 <https://projects.xivo.solutions/issues/2667>`_ - Migrate full stat to akka-stream

**Web Assistant**

* `#2099 <https://projects.xivo.solutions/issues/2099>`_ - Space lookup in web assistant directory

  .. important:: **Behavior change** You can now use spaces in the UCAssistant search field, the searching mechanism is now identical for UCAssistant and CCAgent (note: you cannot search multiple words yet due to back-end restrictions).


**XUC Server**

* `#2658 <https://projects.xivo.solutions/issues/2658>`_ - Be able to get the Fail destination of a given queue
* `#2661 <https://projects.xivo.solutions/issues/2661>`_ - Double ringbacktone on api dial for WEB RTC - Target Version 2019.06

**XiVO PBX**

* `#2662 <https://projects.xivo.solutions/issues/2662>`_ - XDS - DB on MDS can't be connected through unix socket after upgrade from Borealis
