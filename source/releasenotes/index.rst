.. _xivosolutions_release:

*************
Release Notes
*************

.. _deneb_release:

Deneb (2019.12)
===============

Below is a list of *New Features* and *Behavior Changes* compared to the previous LTS version, Callisto (2019.05).


New Features
------------

**Assistants:**

* *Common features*:

  * 3-parties conference for WebRTC
  * Can search with spaces
  * Searches for XIVO users (users on the same XiVO) are insensitive to spaces and diacritics (accented characters)
* *UC Assistant*:

  * Rework of the UC design - see :ref:`uc-assistant`:

    * Call control moved in the application header to always be available whatever the selected tab
    * New Conversation menu to display chat conversations
  * Chat: new Instant messaging feature - see :ref:`uc-assistant_chat`
  * History:

    * Successive calls from the same user are aggregated. You can expand call entry to display more details - see :ref:`uc_assistant_call_history`
    * Defaults to 7 days of history - see :ref:`webassistant_history_nbdays`
* *CC Agent*:

  * Manage the activity's failed destination: can display the destination and change it to a sound or a default queue - see :ref:`agent_activity_failed_dst`
  * Can open an external directory by setting an URL in the remote configuration - see :ref:`agent_external_directory_feature`

**Desktop Assistant**:

* User interface toggle (between UC Assistant and CC Agent) inside the parameter menu - see :ref:`dapp_user_interface`
* Can use a :file:`xivoconfig.ini` file to pre-configure the assistant - see :ref:`dapp_inifile`
* Autolog a user with his token - see :ref:`dapp_autolog_wtoken`
* Improved support of screen resolutions and zoom factors (specially on multi-display configuration)

**Other Applications**:

* CC Manager:

  * Manage the activity's failed destination: can display the destination and change it to a sound or a default queue - see :ref:`ccmanager_activity_failed_dst`
* Rights:

  * New *dissuasion* access rights to grant activity's failed destination management - see :ref:`profile_mgt-profile`

**Reporting**:

* ELK migrated to version 7


**XiVO PBX**:

* Can import WebRTC user - see columns in :ref:`user_import`
* Can import/create entries in the :ref:`phonebook` with a leading ``+`` in phone numbers


**System**:

  * (*XiVO CC*) Postgresql data are not in a volume anymore. It was migrated to the host machine. Therefore the ``pgxivocc`` container is now stateless (it can be removed and re-created without any harm)

Behavior Changes
----------------

**Assistants**:

* *Common features*:
* *UC Assistant*:

  * Rework of the UC design - see :ref:`uc-assistant`:

    * Call control moved in the application header to always be available whatever the selected tab
    * New Conversation menu to display chat conversations
  * History:

    * Successive calls from the same user are aggregated. You can expand call entry to display more details - see :ref:`uc_assistant_call_history`
    * Defaults to 7 days of history - see :ref:`webassistant_history_nbdays`


**Reporting**:

* ELK stack was migrated to the version 7. There's a manual procedure to temporarily enable the previous version - see :ref:`elk7_upgrade_notes`
* Elasticseach is feeded by Logstash every 1 min (`db_replic` is not used for Elasticsearch anymore) - see :ref:`totem_data_flow`
* Elasticsearch is configured to keep 1 month of data (previously it was 7 days) - see :ref:`totem_data_flow`

**System**:

* *XiVO PBX*: nginx and webi are now run inside a container - see :ref:`nginx`:

  * Customization of the services to be used through nginx are no longer supported
* *XiVO CC*:

  * Postgresql data are not in a volume anymore. It was migrated to the host machine. Therefore the ``pgxivocc`` container is now stateless (it can be removed and re-created without any harm)
  * During upgrade new dockezr network ``xivocc_default`` will be created using 172.18.0.0/16 network



Upgrade
-------

.. warning:: **Don't forget** the specific steps to upgrade to another LTS version - see :ref:`upgrade_lts_manual_steps`

Follow the usual upgrade procedures:

* :ref:`XiVO PBX upgrade procedure <upgrade>`
* :ref:`XiVO CC upgrade procedure <upgrade_cc>`
* :ref:`XDS upgrade procedure <upgrade_xds>`


Deneb Bugfixes Versions
=======================

Components version table
------------------------

Table listing the current version of the components.

+----------------------+----------------+
| Component            | current ver.   |
+======================+================+
| **XiVO**                              |
+----------------------+----------------+
| XiVO PBX             | 2019.12.00     |
+----------------------+----------------+
| config_mgt           | 2019.12.00     |
+----------------------+----------------+
| db                   | 2019.12.00     |
+----------------------+----------------+
| outcall              | 2019.12.00     |
+----------------------+----------------+
| db_replic            | 2019.12.00     |
+----------------------+----------------+
| **XiVO CC**                           |
+----------------------+----------------+
| elasticsearch        | 1.7.2          |
+----------------------+----------------+
| kibana_volume        | 2019.12.00     |
+----------------------+----------------+
| nginx                | 2019.12.00     |
+----------------------+----------------+
| pack-reporting       | 2019.12.00     |
+----------------------+----------------+
| pgxivocc             | 1.3            |
+----------------------+----------------+
| recording-rsync      | 1.0            |
+----------------------+----------------+
| recording-server     | 2019.12.00     |
+----------------------+----------------+
| spagobi              | 2019.12.00     |
+----------------------+----------------+
| xivo-full-stats      | 2019.12.00     |
+----------------------+----------------+
| xuc                  | 2019.12.00     |
+----------------------+----------------+
| xucmgt               | 2019.12.01     |
+----------------------+----------------+

Deneb.00
--------

.. note:: **LTS Release**. New features and behavior changes are listed above under the :ref:`deneb_release` section.

Consult the `Deneb (2019.12) Roadmap <https://projects.xivo.solutions/versions/140>`_.

Components updated: **recording-server**, **xivo**, **xivo-confd**, **xivo-full-stats**, **xivo-upgrade**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**CCAgent**

* `#2864 <https://projects.xivo.solutions/issues/2864>`_ - ccagent container resizable is broken

**CCManager**

* `#2840 <https://projects.xivo.solutions/issues/2840>`_ - On CCManager, in the Agent View, I have to scroll down to see the login/logout dropdown-menu entirely

**Chat**

* `#2578 <https://projects.xivo.solutions/issues/2578>`_ - Chat - Choice of a chat server
* `#2716 <https://projects.xivo.solutions/issues/2716>`_ - Install Mattermost
* `#2853 <https://projects.xivo.solutions/issues/2853>`_ - Create Mattermost user when a xivo user is connecting
* `#2856 <https://projects.xivo.solutions/issues/2856>`_ - Login xivo User in Mattermost when connecting
* `#2869 <https://projects.xivo.solutions/issues/2869>`_ - Install Mattermost - Create Personal access token

**Desktop Assistant**

* `#2852 <https://projects.xivo.solutions/issues/2852>`_ - scale the desktop assistant so it can be displayed on every resolution
* `#2866 <https://projects.xivo.solutions/issues/2866>`_ - desktop assistant crash when wrong url or no connection
* `#2871 <https://projects.xivo.solutions/issues/2871>`_ - when we hangup a call, the height of the conversation tab changes for a few seconds
* `#2872 <https://projects.xivo.solutions/issues/2872>`_ - desktop app crash when minimized on windows
* `#2875 <https://projects.xivo.solutions/issues/2875>`_ - ccagent media query is broken due to old code
* `#2878 <https://projects.xivo.solutions/issues/2878>`_ - Desktop application doesn't display correctly when using multi-monitor with different zooms on each of them

**Recording**

* `#2858 <https://projects.xivo.solutions/issues/2858>`_ - Add logs to histoy api or recording server

**Reporting**

* `#2584 <https://projects.xivo.solutions/issues/2584>`_ - Project - Improve call tracking for reporting
* `#2616 <https://projects.xivo.solutions/issues/2616>`_ - As a reporting user I want to see the number of second call initiated by an agent in a queue
* `#2617 <https://projects.xivo.solutions/issues/2617>`_ - As a reporting user I want to see the numbers of transferred consultation calls of an agent in a queue
* `#2801 <https://projects.xivo.solutions/issues/2801>`_ - ELK - Enhance ELK 7 configuration
* `#2816 <https://projects.xivo.solutions/issues/2816>`_ - Add recovery mechanism to streaming part of stats
* `#2849 <https://projects.xivo.solutions/issues/2849>`_ - Missing callee number in call channel for attended transfer
* `#2865 <https://projects.xivo.solutions/issues/2865>`_ - Blind transfers to User must be seen as administrative call in xc_call_channel

**Web Assistant**

* `#2773 <https://projects.xivo.solutions/issues/2773>`_ - Add an error message when no audio / mic device  is plugged when using webrtc
* `#2789 <https://projects.xivo.solutions/issues/2789>`_ - "Play audio" signal from keyboard or headset plays the xivo ringing sound
* `#2859 <https://projects.xivo.solutions/issues/2859>`_ - ConferencePartyEvent Update are not merged but deleted/recreated
* `#2863 <https://projects.xivo.solutions/issues/2863>`_ - Wrong display on search result tab in UC Assistant
* `#2870 <https://projects.xivo.solutions/issues/2870>`_ - merge and refactor containerResizable directive
* `#2896 <https://projects.xivo.solutions/issues/2896>`_ - Conference participants are not updated

**WebRTC**

* `#2038 <https://projects.xivo.solutions/issues/2038>`_ - Create xc_webrtc conference
* `#2882 <https://projects.xivo.solutions/issues/2882>`_ - Support hold on webrtc conference

**XiVO PBX**

* `#2691 <https://projects.xivo.solutions/issues/2691>`_ - [XiVO Phonebook] Phonebook numbers like +XX (E.164 format) should be allowed
* `#2719 <https://projects.xivo.solutions/issues/2719>`_ - Be able to import and export a WebRTC user via CSV
* `#2800 <https://projects.xivo.solutions/issues/2800>`_ - Finish dockerization of webi
* `#2814 <https://projects.xivo.solutions/issues/2814>`_ - Be able to use XiVO UC with dockerized nginx
* `#2851 <https://projects.xivo.solutions/issues/2851>`_ - Dockerize swagger
* `#2868 <https://projects.xivo.solutions/issues/2868>`_ - Externalize nginx logs on hosts
* `#2880 <https://projects.xivo.solutions/issues/2880>`_ - Can import a user with webrtc activated
* `#2881 <https://projects.xivo.solutions/issues/2881>`_ - Can export the webrtc toggle of created users
* `#2892 <https://projects.xivo.solutions/issues/2892>`_ - In User configuration page, remove reference to XiVO Client Login

**XiVOCC Infra**

* `#2860 <https://projects.xivo.solutions/issues/2860>`_ - docker-ce installation breaks

Deneb Intermediate Versions
===========================

.. toctree::
   :maxdepth: 2

   deneb_iv
