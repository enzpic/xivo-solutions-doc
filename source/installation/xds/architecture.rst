.. _xds_architecture:

****************
XDS Architecture
****************

The following diagram presents the XDS architecture with:

* one XiVO Main
* one CTI / Reporting Server (XiVO CC)
* and three MDS


.. figure:: xds-architecture.png
   :scale: 100%


As you can see:

* each MDS and the Main are linked together via an internal SIP trunk (the yellow lines) - via the VoIP IP Address of the MDS
* the *xuc* components of the CTI / Reporting Server is connected to each MDS AMI - via the VoIP IP Address of the MDS
