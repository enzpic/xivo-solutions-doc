*************************
Upgrade Callisto to Deneb
*************************

In this section is listed the manual steps to do when migrating from Callisto to Deneb.

Before Upgrade
==============

On XiVO PBX
-----------

On XiVOCC
---------

.. warning::

  * **You MUST upgrade to** ``docker-ce`` **with the manual procedure below.**
  * Postgres database data will be moved outside the docker volume to the host.
  * When installing the new ``xivocc-installer`` package the XiVOCC services will be stopped.
  * Totem panels will be lost during upgrade. Please check :ref:`elk7_upgrade_notes` page.

* Postgres: database data will be moved from the docker volume to the host in :file:`/var/lib/postgresql/data`.
  This data migration will be almost instantaneous if the volume data is in the same partition.
  To check if the volume data is on the same partition as the host destination :file:`/var/lib/postgresql/data`:

  * Retrieve the pgxivocc volume data dir:

    .. code-block:: bash

     docker inspect xivocc_pgxivocc_1 -f '{{range .Mounts}}{{if (eq .Destination "/var/lib/postgresql/data")}}{{.Source}}{{end}}{{end}}'

  * Then check if it in the same partition as :file:`/var/lib/postgresql/data` dir.
* Docker: you **MUST** upgrade to ``docker-ce`` before upgrading ``xivocc-installer``.

  * Check if you have `docker-engine` installed:

   .. code-block:: bash

    dpkg -l |grep '^ii' |grep docker-engine -q && echo -e "\n\tDocker is installed via docker-engine.\n\tYou MUST upgrade it, BEFORE updating xivocc-installer.\n"

  * If *docker-engine* is installed, you must upgrade to ``docker-ce`` **before** updating *xivocc-installer*:

    * Install ``xivo-dist``:

     .. code-block:: bash

      # Update XiVO sources list
      echo "deb http://mirror.xivo.solutions/debian xivo-deneb main" > /etc/apt/sources.list.d/xivo-dist.list
      apt-get update
      # Install xivo-dist (to prepare docker installation)
      apt-get install xivo-dist
      # Fix docker sources list if needed
      [ $(lsb_release -cs) == "jessie" ] && echo "deb https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
      apt-get update

    * Install ``docker-ce``:

     .. warning:: if you are upgrading from Aldebaran or lower, this step will trigger the upgrade of ``xivocc-installer`` package.
       You MUST NOT do it if the XiVO IPBX upgrade is not finished - as stated in *Aldebaran to Boréalis Upgrade notes* in :ref:`upgrade_lts_manual_steps`.

     .. code-block:: bash

      apt-get install docker-ce

  * Then, you can follow the normal :ref:`upgrade_cc` process.

* Totem panels: during upgrade the ELK stack will be upgraded.

  * Old Elasticsearch/Kibana containers will be removed.
  * Previous data/dashboard won't be upgraded to new version. Therefore they are saved in a folder :file:`/var/local/elasticsearch-1.7`.
  * Be sure to have enough disk space in this folder partition. You should need at *most* 1GB of free disk space. The upgrade script checks the real required space and stops if the space is not available.
  * See also :ref:`elk7_upgrade_notes` page.


After Upgrade
=============

On XiVO PBX
-----------


On XiVOCC
---------

* Totem panels: during upgrade the ELK stack was upgraded.

  * You **MUST** import the Kibana configuration and demo dashboard in the new Kibana - see :ref:`totem_panels_configuration`
  * Old data/dashboard were saved in folder :file:`/var/local/elasticsearch-1.7`.
  * If you had custom Dashboard/Totem panel you must follow the procedure described in :ref:`elk7_upgrade_notes`

