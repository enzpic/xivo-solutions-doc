*****************
Phone Integration
*****************

XUC based web applications like agent interface or UC Assistant integrates buttons for phone control. This section
details necessary configuration, supported phones and limitations.

.. note:: The VoIP VLAN network have to be accessible by the xivocc xuc server

.. _phone_integration_installation:

Required configuration
======================

The following steps are not required if you updated the Provisioning plugins.

Polycom phones
--------------

.. warning::
   This is required only for plugins:

   - xivo-polycom-4.0.9 version below v1.9
   - xivo-polycom-5.4.3 version below v1.8


To enable phone control buttons on web interfaces you must update the basic template of Polycom phones:

- go to the plugin directory: `/var/lib/xivo-provd/plugins/xivo-polycom-VERSION`
- copy the default template from `templates/base.tpl` to `var/templates/`
- then you must update `app.push` parameters in the else section **(do not replace switchboard settings)** as follows:

.. code-block:: ini

    apps.push.messageType="5"
    apps.push.username="guest"
    apps.push.password="guest"

Snom phones
-----------

For transfer to work on Polaris version you must have plugins with version v2.2 or above.


Yealink phones
--------------

.. warning::
   This is required only for plugins xivo-yealink-v80 below v1.31

To enable phone control buttons on web interfaces you must update the basic template of Yealink phones:

- go to the plugin directory: `/var/lib/xivo-provd/plugins/xivo-yealink-VERSION`
- copy the default template from `templates/base.tpl` to `var/templates/`
- enable sip notify even for non switchboard profiles **(do not replace switchboard settings)**

.. code-block:: ini

        {% if XX_options['switchboard'] -%}
        push_xml.sip_notify = 1
        call_waiting.enable = 0
        {% else -%}
        push_xml.sip_notify = 1
        call_waiting.enable = 1
        {% endif %}


Update Device Configuration
---------------------------
- to update device configuration you must run :command:`xivo-provd-cli -c 'devices.using_plugin("xivo-polycom-VERSION").reconfigure()'`
- and finally you must resynchronize the device: :command:`xivo-provd-cli -c 'devices.using_plugin("xivo-polycom-VERSION").synchronize()'`
- refer to provisioning_ documentation for more details
- if the phone synchronization fails check if the phone uses the version of the plugin you have updated, you can use
  :command:`xivo-provd-cli -c 'devices.find()'`

.. _provisioning : http://documentation.xivo.fr/en/stable/administration/provisioning/adv_configuration.html

.. _phone_integration_limitations:

Known limitations
=================

Phone integration with Agent and Web / Desktop application has these limitations:

Transfer
--------

- If the second call was initiated from Agent / Assistant and the called user rejected the call, the first call will stay hold until it is manually resumed
- If the second call was initiated from the phone, the transfer must be also completed from the phone. It can't be completed from Agent / Assistant.
- You cannot complete a transfer initiated from the Agent / Assistant by hanging up.


Conference with Yealink / Polycom
---------------------------------

- Conference can't be created from Agent or Web / Desktop Assistant

