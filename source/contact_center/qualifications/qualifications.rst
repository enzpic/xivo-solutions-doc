.. _call_qualification:

*******************
Call Qualifications
*******************

Call qualifications are used to describe a call in a queue with a certain qualification and its related sub qualification.
For example, a call can be qualified as a qualification **Sales** and a sub qualification **Hardware**.

Qualifications are managed on the
:menuselection:`Services --> Call Center --> Qualifications` page.

.. figure:: qualifications_add.png
   :scale: 85%

:menuselection:`Services --> Call Center --> Qualifications --> Add`

Qualification
=============

Qualification is a main description to which a sub qualification belongs.

A qualification can be configured with the following options:

   * Name: used as name for the qualification
   * Sub Qualifications: used as name for the sub qualification

Sub Qualification
=================

A sub qualification can be configured with the following options:

   * Name: used as name for the qualification

.. figure:: qualifications_edit.png
   :scale: 85%

Assign qualification to queue
=============================

In order to retrieve all qualifications for a certain queue, the qualification must be added to that queue.
A queue can be assigned with multiple qualifications and a qualification can be assigned to multiple queues.

To assign a qualification or multiple qualifications to the queue, open

:menuselection:`Services --> Call Center --> Queues --> Edit --> Qualifications tab` page.

.. figure:: qualifications_assign.png
   :scale: 85%

Removing qualifications
=======================

Removing a qualification will remove also all related sub qualifications. The sub qualifications can be removed
by opening the qualification edit page.

  .. note::

     The (sub)qualification will not be deleted from the database, but will be marked as inactive. This is due to the situation
     in which a call has been already qualified with now removed qualification. Therefore the information won't be lost.

Qualification answers
=====================

A call in a queue can be qualified by the qualification assigned to this queue.

Exporting qualification answers
===============================

See :ref:`ccmanager_qualifications` feature in CCmanager.
