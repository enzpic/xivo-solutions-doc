.. _gcu_upgrade:

*******
Upgrade
*******

The XiVO Centralized User Management (XCU) upgrade.

Prepare sources list
====================

Before upgrading you have to create or change your sources list.
It should be located in the file :file:`/etc/apt/sources.list.d/xivo-dist.list`.

Set sources list for upgrade to **latest** version
--------------------------------------------------

To upgrade to the latest version the sources list must point towards *debian* URI and *xivo-polaris* suite::

    deb http://mirror.xivo.solutions/debian/ xivo-polaris main


Set sources list for upgrade to **specific** version
----------------------------------------------------

To upgrade to a **specific** version the sources list must point towards *archive* URI and *xivo-VERSION-latest* suite.

For example if you want to upgrade to **2017.11** version you should have::

    deb http://mirror.xivo.solutions/archive/ xivo-2017.11-latest main

Note the ``/archive/`` and ``-2017.11-latest`` above.

Upgrade steps
=============

Preparing the upgrade
---------------------

Read :ref:`xivosolutions_release` starting from your version to the version you target.

Read :ref:`xcu_version_specific_upgrade` starting from your version to the version you target.

Upgrade
-------

When you have checked the sources.list you can upgrade:

#. Execute with the following commands::

    apt-get update
    apt-get install gcu-installer

#. If are using postgresql installed using sources from `Postgresql Wiki <https://wiki.postgresql.org/wiki/Apt>`_,
   upgrading ``gcu-installer`` may trigger installing new version of PostgreSQL (in parallel to curent one).
   Suggested action is to disable this new version. If for example GCU use postgresql 9.5 and new version
   9.6 was added:

   * stop postgresql 9.6 manually by: ``service postgresql stop 9.6``
   * disable postgresql 9.6 autostart: in ``/etc/postgresql/9.6/main/start.conf`` replace ``auto`` with ``disabled``
   * ensure postgresql 9.5 is running by: ``service postgresql start 9.5``

#. Download the new images::

    docker-compose -p icdu -f /etc/docker/compose/icdu.yml pull

#. And run the new container (**All XCU services will be restarted**)::

    docker-compose -p icdu -f /etc/docker/compose/icdu.yml up -d

.. note::
   Please, ensure your server date is correct before starting. If system date differs too much from correct date, you may get an authentication error preventing download of the docker images.

.. _xcu_version_specific_upgrade:

Version specific XCU upgrade procedures
=======================================

Upgrade from versions before 2018.04
------------------------------------

Docker-CE must be installed instead of Docker Engine.

Upgrade from versions before 2017.06
------------------------------------

If you installed GCU version older than 2017.06, you can upgrade it by following :ref:`Installation <gcu_installation>`
instructions. Further instructions and notices:

1. It is strongly recommended you backup your PostgreSQL ``icx`` database and SSH keys before proceeding.
2. Installation will install new version of configuration files (like ``/etc/docker/compose/icdu.yml``). If some file
   already exists with different content, you be prompted to choose correctly version or merge differences.
   Unless you are sure you need special version, use choice: *install the package maintainer's version*.
3. Installation will reuse your SSH certificate stored in ``/etc/docker/interface-centralisee/ssh_key`` if exists.
4. Installation will reuse PostgreSQL user ``icx`` if exists - make sure it has password ``icx`` and access to database ``icx``.
5. Installation will reuse PostgreSQL database ``icx`` if exists.

Check carefully output of ``apt-get install gcu-installer``, you will be informed about each component reused and about any
manual checks or actions needed, if necessary.
