******
Incall
******

General Configuration
=====================

You can configure incoming calls settings in :menuselection:`Services --> IPBX --> Call Management --> Incoming calls`.


:abbr:`DID (Direct Inward Dialing)` Configuration
-------------------------------------------------

You can use special characters to match extensions. The most useful are::

   . (period): will match one or more characters
   X: will match only one character

You can find more details about pattern matching in Asterisk (hence in XiVO) on
`the Asterisk wiki <https://wiki.asterisk.org/wiki/display/AST/Pattern+Matching>`_.

DID will not be validated against context ranges if pattern matching is used.
Patterns are entered and displayed without the "_" prefix in the web interface.
