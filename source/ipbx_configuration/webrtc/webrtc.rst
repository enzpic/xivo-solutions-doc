******
WebRTC
******

General notes
=============

.. note:: added in version 2016.04

XiVO comes with a WebRTC lines support, you can use in with XiVO *UC Assistant* and *Desktop Assistant*.
Before starting, please check the :ref:`webrtc_requirements`.

Current WebRTC implementation requires following configuration steps:

* configure :ref:`asterisk_http_server`,
* and create user :ref:`with one line configured for WebRTC <configure_user_with_webrtc_line>`. To have user with both SIP and WebRTC line is not supported.

.. _configure_user_with_webrtc_line:

Configuration of user with WebRTC line
======================================

1. Create user

2. Add line to user without any device

3. Edit the line created and, in the *Advanced* tab, add `webrtc=yes` options:

.. figure:: webrtc_line.png
    :scale: 100%

Fallback Configuration
======================

When the user is not connected to its WebRTC line, or disconnect from the assistant, you can route the call to a default number
as for example the user mobile number.
Update the fail option on the No Answer user tab configuration, and add an extension to the appropriate context.

.. figure:: user_fail.png
    :scale: 100%

.. _configure_webrtc_video:

Experimental video call feature
===============================

.. note:: Since 2018.02 as an experimental feature, susceptible to change or removal at any moment.

Since this feature is experimental, it is disabled by default. Video calls are enabled by setting the environment
variable ``ENABLE_VIDEO`` to ``true`` in the
:file:`/etc/docker/compose/custom.env` file::

   ...
   ENABLE_VIDEO=true

Then you need to recreate the xucmgt container with ``xivocc-dcomp up -d xucmgt``.

WebRTC users are automatically configured with VP8 video codec to allow video calls, you must disable all other video
codecs. Please avoid any specific codec configuration for WebRTC peers and be sure to remote all video codecs from
the ``SIP General settings``, tab ``Signalling``, section ``Codecs``. Either do not customize codecs, either do not
select any video codecs.

Manual configuration of user with WebRTC line
==============================================

For the records 

.. toctree::
    
    webrtc_manualconf

